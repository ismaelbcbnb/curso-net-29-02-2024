# Projeto Cadastro de Alunos

## Resolução do exercício 2 da Aula 03

Desenvolva um sistema simples de cadastro de alunos com as classes Aluno e Turma. Permita adicionar alunos à turma e calcular a média da turma.

## Como executar o projeto

- Necessário .NET 6 instalado

`dotnet run`

- Verifique a versão do seu dotnet com o comando: `dotnet --list-sdks` e atualize o arquivo `global.json` com a versão correta.
