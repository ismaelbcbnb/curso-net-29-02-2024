public class Professor : Pessoa
{
    private List<ITurma> _turmas { get; set; }

    public Professor(string nome, int idade) : base(nome, idade)
    {
        Nome = nome;
        _turmas = new List<ITurma>();
    }

    public void AdicionarTurma(ITurma turma)
    {
        _turmas.Add(turma);
    }

    public void RemoverTurma(ITurma turma)
    {
        _turmas.Remove(turma);
    }

    public List<ITurma> ListarTurmas()
    {
        return _turmas;
    }

    public override void FazerAniversario()
    {
        throw new NotImplementedException();
    }
}