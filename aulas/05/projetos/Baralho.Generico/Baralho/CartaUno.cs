public class CartaUno : Carta<Uno>
{
    public string Cor { get; set; }

    public CartaUno(string nome, string cor) : base(new Uno(), nome)
    {
        Cor = cor;
    }

        public override string ToString()
    {
        return $"{Nome}, {Cor} de {Tipo}";
    }
}