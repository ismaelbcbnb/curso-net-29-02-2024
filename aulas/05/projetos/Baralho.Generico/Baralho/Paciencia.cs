public class Paciencia : ITipo
{
    public string Nome { get; set; } = "Paciência";

    public override string ToString()
    {
        return Nome;
    }
}