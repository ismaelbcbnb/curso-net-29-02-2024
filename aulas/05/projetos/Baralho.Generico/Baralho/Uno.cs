public class Uno : ITipo
{
    public string Nome { get; set; } = "Uno";

    public override string ToString()
    {
        return Nome;
    }
}