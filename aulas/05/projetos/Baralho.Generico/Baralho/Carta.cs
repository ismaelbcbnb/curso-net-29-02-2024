public abstract class Carta<T> where T : ITipo
{
    public T Tipo { get; set; }
    public string Nome { get; set; }

    public Carta(T tipo, string nome)
    {
        Tipo = tipo;
        Nome = nome;
    }

    public override string ToString()
    {
        return $"{Nome} de {Tipo}";
    }

    public override bool Equals(object obj)
    {
        if (obj is Carta<T> carta)
        {
            return Nome == carta.Nome && Tipo.Equals(carta.Tipo);
        }
        return false;
    }
}