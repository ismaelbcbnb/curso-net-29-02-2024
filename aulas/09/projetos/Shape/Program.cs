﻿
using Bogus;

static HashSet<string> gerarNomes(int quantidade)
{
    var faker = new Faker();
    var nomes = new HashSet<string>();

    while (nomes.Count < quantidade)
    {
        nomes.Add(faker.Name.FullName());
    }

    return nomes;
}

var faker = new Faker();
var devedores = gerarNomes(60000);

var eDevedor = devedores.Contains(faker.Name.FullName());
Console.WriteLine(eDevedor);


