# Revisão de lógica de programação com algoritmos, estruturas de dados, fluxo de controle e funções

## Objetivos da aula

- Relembrar os conceitos básicos de lógica de programação
- Conhecer os principais tipos de algoritmos e estruturas de dados
- Aprender a usar o fluxo de controle para controlar o fluxo de execução de um programa
- Aprender a usar funções para modularizar e reutilizar o código

## Conteúdo

### Matéria

### Algoritmos

- Definição: um algoritmo é uma sequência finita de passos que descreve como resolver um problema ou realizar uma tarefa
- Exemplos: algoritmo para calcular a média de duas notas, algoritmo para ordenar uma lista de números, algoritmo para encontrar o maior elemento de um vetor
- Propriedades: um algoritmo deve ser correto, eficiente, claro e finito
- Representação: um algoritmo pode ser representado de diferentes formas, como pseudocódigo, fluxograma, linguagem natural ou linguagem de programação

### Estruturas de dados

- Definição: uma estrutura de dados é uma forma de organizar e armazenar dados na memória de um computador
- Exemplos: variáveis, vetores, matrizes, listas, pilhas, filas, árvores, grafos, tabelas hash, etc
- Propriedades: uma estrutura de dados deve ser adequada ao tipo e à quantidade de dados que se quer armazenar, bem como às operações que se quer realizar sobre esses dados
- Representação: uma estrutura de dados pode ser representada de diferentes formas, como diagramas, tabelas, notações, etc

> **Vetores** - A principal diferença entre um vetor e uma lista é que um vetor tem um tamanho fixo, definido na sua criação, enquanto uma lista tem um tamanho dinâmico, que pode aumentar ou diminuir conforme a necessidade. Além disso, um vetor é um tipo primitivo, que faz parte da linguagem C#, enquanto uma lista é um tipo genérico, que faz parte da biblioteca de classes do .NET

```c#
int[] numeros = new int[5] {1, 2, 3, 4, 5}; // vetor original
int[] novoNumeros = new int[6]; // novo vetor com tamanho maior
Array.Copy(numeros, novoNumeros, numeros.Length); // copia os elementos do vetor original para o novo vetor
novoNumeros[5] = 6; // adiciona um novo elemento no final do novo vetor
```

```c#
List<int> numeros = new List<int>() {1, 2, 3, 4, 5}; // lista original
numeros.Add(6); // adiciona um novo elemento no final da lista
numeros.Remove(3); // remove o elemento 3 da lista
```

> **Matrizes** - A diferença entre Matriz e Vetor é que um vetor é uma matriz unidimensional, ou seja, tem apenas uma dimensão, enquanto uma matriz pode ter mais de uma dimensão, como duas, três ou mais. Você pode declarar um vetor usando colchetes após o tipo dos elementos, por exemplo:

```c#
int[] numeros = new int[5]; // vetor de inteiros com tamanho 5
string[] nomes = new string[3] {"João", "Maria", "Pedro"}; // vetor de strings com tamanho 3 e valores iniciais
```

Você pode declarar uma matriz usando colchetes após o tipo dos elementos para cada dimensão, por exemplo:

```c#
int[,] multiDimensionalArray1 = new int[2, 3]; // matriz bidimensional de inteiros com tamanho 2 x 3
int[,,] multiDimensionalArray2 = new int[2, 3, 4]; // matriz tridimensional de inteiros com tamanho 2 x 3 x 4
```

> **Pilhas** - Uma pilha é uma estrutura de dados que armazena uma coleção de objetos do último a entrar, primeiro a sair. Isso significa que o último objeto que foi adicionado à pilha é o primeiro a ser removido dela. Uma pilha pode ser usada para implementar recursos como o botão desfazer, a navegação de páginas ou a chamada de funções.

```c#
using System;
using System.Collections;

class Program
{
    static void Main(string[] args)
    {
        // Criar uma pilha vazia
        Stack pilha = new Stack();

        // Adicionar objetos na pilha
        pilha.Push("A");
        pilha.Push("B");
        pilha.Push("C");

        // Mostrar o número de objetos na pilha
        Console.WriteLine("A pilha tem {0} objetos", pilha.Count);

        // Mostrar o objeto no topo da pilha
        Console.WriteLine("O objeto no topo da pilha é {0}", pilha.Peek());

        // Remover e mostrar o objeto no topo da pilha
        Console.WriteLine("O objeto removido da pilha é {0}", pilha.Pop());

        // Verificar se um objeto está na pilha
        Console.WriteLine("A pilha contém o objeto A? {0}", pilha.Contains("A"));

        // Iterar sobre os objetos da pilha
        Console.WriteLine("Os objetos da pilha são:");
        foreach (var obj in pilha)
        {
            Console.WriteLine(obj);
        }

        // Remover todos os objetos da pilha
        pilha.Clear();
        Console.WriteLine("A pilha está vazia? {0}", pilha.Count == 0);
    }
}
```

Saída:

```texto
A pilha tem 3 objetos
O objeto no topo da pilha é C
O objeto removido da pilha é C
A pilha contém o objeto A? True
Os objetos da pilha são:
B
A
A pilha está vazia? True
```

> **Filas** Uma fila é uma estrutura de dados que armazena uma coleção de objetos na ordem primeiro a entrar, primeiro a sair (FIFO). Isso significa que o primeiro objeto que entra na fila é o primeiro que sai dela. Uma fila pode ser usada para implementar recursos como o botão desfazer, a navegação de páginas ou a chamada de funções.

```c#
using System;
using System.Collections;

class Program
{
    static void Main(string[] args)
    {
        // Criar uma fila vazia
        Queue fila = new Queue();

        // Adicionar objetos na fila
        fila.Enqueue("A");
        fila.Enqueue("B");
        fila.Enqueue("C");

        // Mostrar o número de objetos na fila
        Console.WriteLine("A fila tem {0} objetos", fila.Count);

        // Mostrar o objeto no início da fila
        Console.WriteLine("O objeto no início da fila é {0}", fila.Peek());

        // Remover e mostrar o objeto no início da fila
        Console.WriteLine("O objeto removido da fila é {0}", fila.Dequeue());

        // Verificar se um objeto está na fila
        Console.WriteLine("A fila contém o objeto A? {0}", fila.Contains("A"));

        // Iterar sobre os objetos da fila
        Console.WriteLine("Os objetos da fila são:");
        foreach (var obj in fila)
        {
            Console.WriteLine(obj);
        }

        // Remover todos os objetos da fila
        fila.Clear();
        Console.WriteLine("A fila está vazia? {0}", fila.Count == 0);
    }
}
```

Saída:

```texto
A fila tem 3 objetos
O objeto no início da fila é A
O objeto removido da fila é A
A fila contém o objeto A? False
Os objetos da fila são:
B
C
A fila está vazia? True
```

> **Árvores** - Uma árvore é uma estrutura de dados que armazena uma coleção de objetos hierarquicamente. Cada objeto na árvore é chamado de nó, e cada nó pode ter zero ou mais nós filhos. O nó que não tem nenhum nó pai é chamado de raiz, e os nós que não têm nenhum nó filho são chamados de folhas. Uma árvore pode ser usada para representar dados que têm uma relação de ancestralidade ou de subordinação, como uma árvore genealógica, uma árvore de diretórios ou uma árvore de expressões.

```c#
using System;
using System.Windows.Forms;

class Program
{
    static void Main(string[] args)
    {
        // Criar uma árvore com três nós
        TreeNode root = new TreeNode("Raiz");
        TreeNode node1 = new TreeNode("Nó 1");
        TreeNode node2 = new TreeNode("Nó 2");
        root.Nodes.Add(node1);
        root.Nodes.Add(node2);

        // Mostrar o texto e o número de filhos de cada nó
        Console.WriteLine("O nó raiz tem o texto {0} e {1} filhos", root.Text, root.Nodes.Count);
        Console.WriteLine("O nó 1 tem o texto {0} e {1} filhos", node1.Text, node1.Nodes.Count);
        Console.WriteLine("O nó 2 tem o texto {0} e {1} filhos", node2.Text, node2.Nodes.Count);

        // Mostrar o pai e os irmãos de cada nó
        Console.WriteLine("O nó raiz tem o pai {0} e os irmãos {1}", root.Parent, root.NextNode);
        Console.WriteLine("O nó 1 tem o pai {0} e os irmãos {1} e {2}", node1.Parent.Text, node1.PrevNode, node1.NextNode.Text);
        Console.WriteLine("O nó 2 tem o pai {0} e os irmãos {1} e {2}", node2.Parent.Text, node2.PrevNode.Text, node2.NextNode);

        // Criar um controle TreeView para exibir a árvore
        TreeView treeView = new TreeView();
        treeView.Nodes.Add(root);
        treeView.ExpandAll();

        // Criar um formulário para exibir o controle TreeView
        Form form = new Form();
        form.Text = "Exemplo de árvore em C#";
        form.Controls.Add(treeView);
        form.ShowDialog();
    }
}
```

Saída:

```texto
O nó raiz tem o texto Raiz e 2 filhos
O nó 1 tem o texto Nó 1 e 0 filhos
O nó 2 tem o texto Nó 2 e 0 filhos
O nó raiz tem o pai  e os irmãos 
O nó 1 tem o pai Raiz e os irmãos  e Nó 2
O nó 2 tem o pai Raiz e os irmãos Nó 1 e 
```

> **Grafos** - Um grafo é uma estrutura de dados que representa um conjunto de objetos e as relações entre eles. Cada objeto é chamado de vértice ou nó, e cada relação é chamada de aresta ou arco. Um grafo pode ser usado para modelar diversos problemas, como redes sociais, mapas, rotas, circuitos, árvores, etc.
> Um grafo pode ser classificado de acordo com algumas propriedades, como:
>
> - Direcionado ou não direcionado: um grafo é direcionado se as arestas têm uma direção, ou seja, indicam um sentido entre os
> vértices. Um grafo é não direcionado se as arestas não têm uma direção, ou seja, indicam uma conexão bidirecional entre os
> vértices.
>
> - Ponderado ou não ponderado: um grafo é ponderado se as arestas têm um peso, ou seja, um valor numérico associado a elas. Um
> grafo é não ponderado se as arestas não têm um peso, ou seja, são todas iguais.
> - Cíclico ou acíclico: um grafo é cíclico se existe um caminho que começa e termina no mesmo vértice, passando por outros
> vértices. Um grafo é acíclico se não existe nenhum caminho desse tipo.
> - Conexo ou desconexo: um grafo é conexo se existe um caminho entre quaisquer dois vértices. Um grafo é desconexo se existem
> vértices que não podem ser alcançados a partir de outros vértices.

```c#
using System;
using QuickGraph;

class Program
{
    static void Main(string[] args)
    {
        // Criar um grafo não direcionado e não ponderado
        Graph grafo = new Graph();

        // Adicionar vértices no grafo
        Vertex v1 = new Vertex("A");
        Vertex v2 = new Vertex("B");
        Vertex v3 = new Vertex("C");
        Vertex v4 = new Vertex("D");
        grafo.AddVertex(v1);
        grafo.AddVertex(v2);
        grafo.AddVertex(v3);
        grafo.AddVertex(v4);

        // Adicionar arestas no grafo
        Edge e1 = new Edge(v1, v2);
        Edge e2 = new Edge(v2, v3);
        Edge e3 = new Edge(v3, v4);
        Edge e4 = new Edge(v4, v1);
        grafo.AddEdge(e1);
        grafo.AddEdge(e2);
        grafo.AddEdge(e3);
        grafo.AddEdge(e4);

        // Mostrar o número de vértices e arestas do grafo
        Console.WriteLine("O grafo tem {0} vértices e {1} arestas", grafo.VertexCount, grafo.EdgeCount);

        // Mostrar os vértices e as arestas do grafo
        Console.WriteLine("Os vértices do grafo são:");
        foreach (var v in grafo.Vertices)
        {
            Console.WriteLine(v);
        }
        Console.WriteLine("As arestas do grafo são:");
        foreach (var e in grafo.Edges)
        {
            Console.WriteLine(e);
        }
    }
}
```

Saída:

```texto
O grafo tem 4 vértices e 4 arestas
Os vértices do grafo são:
A
B
C
D
As arestas do grafo são:
A -> B
B -> C
C -> D
D -> A
```

Leia mais:

[Vetores com C#](https://learn.microsoft.com/pt-br/dotnet/api/system.numerics.vector-1?view=net-8.0)

[Listas com .NET](https://learn.microsoft.com/pt-br/dotnet/csharp/tour-of-csharp/tutorials/arrays-and-collections)

[Pilhas em C# (Stack)](https://learn.microsoft.com/pt-br/dotnet/api/system.collections.stack?view=net-8.0)

[Filas em C# (Queue)](https://learn.microsoft.com/pt-br/dotnet/api/system.collections.queue?view=net-8.0)

[Árvores em C#](https://learn.microsoft.com/pt-br/dotnet/csharp/advanced-topics/expression-trees/expression-trees-explained)

[Grafos em C#](https://learn.microsoft.com/pt-br/dotnet/api/microsoft.visualstudio.graphmodel.graph?view=visualstudiosdk-2022)

### Fluxo de controle

- Definição: o fluxo de controle é a ordem em que as instruções de um programa são executadas
- Exemplos: fluxo sequencial, fluxo condicional, fluxo iterativo, fluxo recursivo, etc
- Propriedades: o fluxo de controle deve ser coerente, lógico e controlável
- Representação: o fluxo de controle pode ser representado por meio de estruturas de controle, como if, else, switch, for, while, do-while, break, continue, return, etc

### Funções

- Definição: uma função é um bloco de código que recebe um ou mais parâmetros de entrada, realiza uma determinada tarefa e retorna um ou mais valores de saída
- Exemplos: função para calcular o fatorial de um número, função para verificar se um número é primo, função para converter uma temperatura de Celsius para Fahrenheit, etc
- Propriedades: uma função deve ter um nome, uma assinatura, um corpo e um retorno
- Representação: uma função pode ser representada por meio de uma declaração, uma definição, uma chamada e um retorno

## Exercícios práticos

- Escreva um algoritmo em pseudocódigo que receba dois números inteiros e imprima a soma deles
- Escreva um algoritmo em pseudocódigo que receba um vetor de números inteiros e imprima o menor e o maior elemento do vetor
- Escreva um algoritmo em pseudocódigo que receba um número inteiro positivo e imprima se ele é par ou ímpar
- Escreva um algoritmo em pseudocódigo que receba um número inteiro positivo e imprima todos os seus divisores
- Escreva um algoritmo em pseudocódigo que receba um número inteiro positivo e imprima a sua tabuada de multiplicação
- Escreva um algoritmo em pseudocódigo que receba uma matriz de números inteiros e imprima a sua transposta
- Escreva um algoritmo em pseudocódigo que receba uma lista de nomes e imprima o nome mais longo e o nome mais curto da lista
- Escreva um algoritmo em pseudocódigo que receba uma pilha de números inteiros e imprima a soma de todos os elementos da pilha
- Escreva um algoritmo em pseudocódigo que receba uma fila de números inteiros e imprima a média de todos os elementos da fila
- Escreva um algoritmo em pseudocódigo que receba uma árvore binária de números inteiros e imprima a soma de todos os elementos da árvore
- Escreva um algoritmo em pseudocódigo que receba um grafo de números inteiros e imprima o grau de cada vértice do grafo
- Escreva um algoritmo em pseudocódigo que receba uma tabela hash de números inteiros e imprima o número de colisões da tabela
- Escreva uma função em pseudocódigo que receba um número inteiro positivo e retorne o seu fatorial
- Escreva uma função em pseudocódigo que receba um número inteiro positivo e retorne se ele é primo ou não
- Escreva uma função em pseudocódigo que receba uma temperatura em Celsius e retorne a sua conversão em Fahrenheit
- Escreva uma função em pseudocódigo que receba dois números inteiros e retorne o seu máximo divisor comum
- Escreva uma função em pseudocódigo que receba dois números inteiros e retorne o seu mínimo múltiplo comum
- Escreva uma função em pseudocódigo que receba um número inteiro positivo e retorne o seu enésimo termo da sequência de Fibonacci
- Escreva uma função em pseudocódigo que receba um número inteiro positivo e retorne o seu dígito mais significativo
- Escreva uma função em pseudocódigo que receba uma string e retorne se ela é um palíndromo ou não

[Link para Portugol](https://dgadelha.github.io/Portugol-Webstudio/)